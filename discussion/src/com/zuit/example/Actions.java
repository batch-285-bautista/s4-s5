package com.zuit.example;

public interface Actions {
    public void sleep();

    public void run();

    /*public void morningGreetings();

    public void holidayGreetings();*/

    //mini activity
    //create a greetings interface
    //morningGreet() and holidayGreet()
    //implement it in Person.java
}
