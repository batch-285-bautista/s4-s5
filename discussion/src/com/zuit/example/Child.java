package com.zuit.example;

public class Child extends Parent{
    public void speak(){
        //we have the original method from the parent class
        super.speak();
        System.out.println("I am a child, I am no parent.");
    }
}
