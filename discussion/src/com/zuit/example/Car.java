package com.zuit.example;

public class Car {
    //Access Modifier
        //these are used to restrict the scope of a class, constructor, variable, method, or data
    //Four types of access modifiers
    /*1. Default - now keyword (Accessibility is within the package)
    * 2. Private - properties or method are only accessible within the class
    *     example: within a Car.java
    * 3. Protected - properties and methods are only accessible by the class of the same package and the subclass present in any package
    *   by the class of the same package and subclasses and classes in the package that is outside the package
    * 4. Public - properties and methods can be accessed anywhere
    *
    * Class Creation
    *   Four parts of class creation
    *   1. Properties - are characteristics of objects
    *   2. Constructor - used to create/instantiate an object
    *       a.Empty Constructor - create an object that doesn't have any args or params
    *           instantiate an object from the car class w/o declaring properties in order for us to do that, we are going to create an empty constructor.
    *       b. parameterized constructor - creates an object with args/params
    *   3. Getters and Setters - get and set the values of each property of the object
    *       a. getters - retrieve the value of an instantiated object
    *       b. setters - used to change the default value of the instantiated object
    *   4. Methods - functions an object can perform or actions*/

    private String name;
    private String brand;

    private int yearOfMake;

    private Driver driver; //driver is on public so that it will no need to import it here

    public Car(){
        this.yearOfMake = 2000;//set a default value upon instantiation

        //added driver
        this.driver = new Driver("Cardi");
    } //empty constructor also known the default constructor

    //parameterized
    public Car (String name, String brand, int yearOfMake){
        this.name = name;
        this.brand = brand;
        this.yearOfMake = yearOfMake;
        this.driver = new Driver ("Cardi");
    }

    //getters
    public String getName(){
        return this.name;
    }

    public String getBrand(){
        return  this.brand;
    }

    public int getYearOfMake(){
        return  this.yearOfMake;
    }

    public String getDriverName(){
        return this.driver.getName();
    }
    //setters - we don't need to return anything
    public void setName( String name){
        this.name = name;
    }

    public void setBrand ( String brand){
        this.brand = brand;
    }

    public void setYearOfMake( int yearOfMake){
        if(yearOfMake <= 2023){
            this.yearOfMake = yearOfMake;
        }
    }
    public void setDriver(String driver){
        this.driver.setName(driver);
    }

    //methods
    public void drive(){
        System.out.println("The car is running. Vroom. Vroom.");
    }



}
