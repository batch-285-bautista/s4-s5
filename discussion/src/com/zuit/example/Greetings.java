package com.zuit.example;

public interface Greetings {
    public void morningGreetings();

    public void holidayGreetings();
}
