package com.zuit.example;

public class Person implements Actions{
    public void sleep(){
        System.out.println("zzzzzzzzzzzzzz...(snore");
    }

    public void run(){
        System.out.println("Running!...");
    }

    public void morningGreetings(){
        System.out.println("Good morning loves. <3");
    }

    public void holidayGreetings(){
        System.out.println("Happy holidays!");
    }
}
