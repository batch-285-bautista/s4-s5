import com.zuit.example.*;
//"*" all class imported

public class Main {
    public static void main(String[] args) {
        //OOP stands for Object-Oriented Programming, is a programming model that allows developers to design software around data or objects rather than function and logic.

        //OOP Concepts
            //Object - abstract idea that represents something in the real world
            //example: the concept of a dog
            //Class - representation of the object using code
            //ExampleL Writing a code that would describe a dog
            //Instance - unique copy of the idea, made "Physical"
                //actual creation the physical copy enables us the copy of concept such as an instance
                //Example: Instantiating a dog named Fluffy from the dog class
        //Inheritance - allows modelling an object that is a subset of another object
        //it defines a relationship
        //Composition - allows modelling objects that are made up of other objects
            //bot entities are dependent on each other
        //composed object cannot exist w/o other entity
        //it defines a relationship

        //Examples: A car is vehicle - inheritance
        //          A car HAS A DRIVER - COMPOSITION

        //Objects
            /*"States ad Attributes" - what is the idea about?
            * Properties of the specific object
            * Dog's name, bread, color, etc.
            * Behaviors - what can the idea do?
            *   What can the actual dog do?
            *   Bark, sit
            *
            * Four Pillar of OOP
            * 1. Encapsulation
            *       -a mechanism if wrapping the data (variables*) and code acting on the data (methods) together as a single unit
            *
            * Data hiding - the variables of a class will be hidden from the other classes, and can be accessed only through the methods of their current class
            * -- ti achieve encapsulation, these are some examples:
            *   //variable/properties as private
            *   //provide a public setter and getter function
             */

        //Create a car
        Car myCar = new Car(); // we accessed the empty/default constructor
        myCar.drive();

        //going to assign the properties of myCar using setter methods
        /*myCar.setName("HiAce");
        myCar.setBrand("Toyota");
        myCar.setYearOfMake((2025));*/
        //2025 is invalid because it was set 2023 that will not be exceeds there and then a default was printed.//My Toyota HiAce was made in 2000.

//        myCar.name; //no result because it is private class
        myCar.setName("Patrol");
        myCar.setBrand("Toyota");
        myCar.setYearOfMake((2023));
        myCar.setDriver("Ali");



        //Mini-Activity - instantiate another car from the car class

        System.out.println("My " + myCar.getBrand() + " " + myCar.getName() + " was made in " +myCar.getYearOfMake() + ". It is driven by " + myCar.getDriverName() + "."); //print with getters
        //My Toyota HiAce was made in 2023.

        //Inheritance - as the process hwere one class acquire the properties and methods of another class
        //with theuse of inheritance, that information is made manageable in hierarchical order

        Dog myPet = new Dog();
        myPet.setName("Bantay");
        myPet.setColor("Brown");
        myPet.speak(); //Aww aww
        System.out.println("My dog is " + myPet.getName() + " and it is a " + myPet.getColor() + " " + myPet.getBreed() + "!"); //My dog is Bantay and it is a Brown Chihuahua!

        myPet.call();

        //Abstraction is a process here logic and complexity are hidden from the user
        //iNTERFACE
            //this is to achieve total abstraction
            //creating abstract classes doesn't support "multiple inheritance" but it can be achieved with interfaces
            //interface allows multiple implementation, list functionalities
            //act as contracts where in a class implements the interface, should have methods that the interfaces has defines in the class

        Person child = new Person();
        child.sleep();

        child.morningGreetings();
        child.holidayGreetings();

        //original method
        StaticPoly myaddition = new StaticPoly();
        System.out.println(myaddition.addition(5, 6));
        //based on arguments
        System.out.println(myaddition.addition(5, 6, 7));
        //based on data types
        System.out.println(myaddition.addition(5.7, 6.6));

        //B. Dynamic or run-time polymorphism
            //function is overridden by replacing the definition of the method in the parent class in the child class

        Child myChild = new Child();
        myChild.speak();
    }

}