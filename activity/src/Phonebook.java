import java.util.ArrayList;

public class Phonebook {
    //declare
    private ArrayList<Contact> contacts;

    //Constructor
    public Phonebook (){
        contacts = new ArrayList<Contact>();
    }

    // Parameterized constructor
    public Phonebook(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    // Getter
    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    // Setter
    public void addContact(Contact contact) {
        contacts.add(contact);
    }
}
