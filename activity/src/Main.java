import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Phonebook myPhoneBook = new Phonebook();

        boolean exit = false;
        while (!exit) {
            // Prompt the user to enter information for the contact
            System.out.print("Name: ");
            String name = scanner.nextLine();
            String[] contactNumbers = new String[2];
            String[] addresses = new String[2];
            for (int i = 0; i < 2; i++) {
                System.out.print("Phone num " + (i+1) + ": ");
                contactNumbers[i] = scanner.nextLine();
            }

            System.out.print("Home Address: ");
            addresses[0] = scanner.nextLine();

            System.out.print("Office Address: ");
            addresses[1] = scanner.nextLine();

            Contact contact = new Contact(name, contactNumbers, addresses);
            myPhoneBook.addContact(contact);

            System.out.print("Do you want to add another contact? (Y/N): ");
            String choice = scanner.nextLine();
            if (choice.equalsIgnoreCase("N")) {
                exit = true;
            }
        }

        if (myPhoneBook.getContacts().isEmpty()) {
            System.out.println("The phone book is empty.");
        } else {
            for (Contact c : myPhoneBook.getContacts()) {
                System.out.println("------------------------------------------");
                System.out.println(c.getName());
                System.out.println("------------------------------------------");
                String[] contactNumbersArray = c.getContactNumbers();
                String[] addressesArray = c.getAddresses();
                System.out.println(c.getName() + " has the following registered numbers:");
                for (int i = 0; i < 2; i++) {
                    System.out.println(contactNumbersArray[i]);

                }
                System.out.println("------------------------------------------");
                System.out.println(c.getName() + " has the following registered addresses:");
                for (int y = 0; y < 2; y++) {
                    if (y == 0) {
                        System.out.println("Home Address: " + addressesArray[y]);
                    } else if (y == 1) {
                        System.out.println("Office Address: " + addressesArray[y]);
                    }
                }

                System.out.println("==========================================");
                System.out.println();
            }
        }

        scanner.close();
    }
}
