public class Contact {
    private String name;
    private String[] contactNumbers;
    private String[] addresses;

    public Contact(String name, String[] contactNumbers, String[] addresses) {
        this.name = name;
        this.contactNumbers = contactNumbers;
        this.addresses = addresses;
    }

    //getters
    public String getName() {
        return name;
    }

    public String[] getContactNumbers() {
        return contactNumbers;
    }
    public String[] getAddresses() {
        return addresses;
    }

    //setters
    public void setName(String name) {
        this.name = name;
    }

    public void setContactNumbers(String[] contactNumbers) {
        this.contactNumbers = contactNumbers;
    }

    public void setAddresses(String[] addresses) {
        this.addresses = addresses;
    }

    /*public void contactInfo(){
        *//*super.contactInfo();*//*
        System.out.println(this.getName() + " is from " + this.getAddress() + ". this is the contact number: " + getContactNumber());
    }*/

}
